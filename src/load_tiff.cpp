#include "load_png.h"

using std::make_unique;

#include <vector>
using std::vector;

#include <string>
using std::string;

#include "Logging.h"
#include "geotiff.h"
#include "xtiffio.h"

#include "Image.h"

extern "C" void tiff_error_handler(const char *module, const char *fmt, va_list args) {
	string mod = module? module : "";
	const int N = 1024;
	char buffer[N];
	int end = vsnprintf(buffer, N, fmt, args);
	buffer[end] = '\0';
	console->error("({}) - {}", mod, buffer );
}

extern "C" void tiff_warning_handler(const char *module, const char *fmt, va_list args) {
	string mod = module ? module : "";
	const int N = 1024;
	char buffer[N];
	int end = vsnprintf(buffer, N, fmt, args);
	buffer[end] = '\0';
	console->warn("({}) - {}", mod, buffer);
}

std::unique_ptr<Image> load_tiff(const std::filesystem::path &path) {
	uint32_t W, H;
	string filename = path.filename().string();
	console->info("Loading image \"{}\" ...", filename);

	TIFF *tif;
	TIFFSetErrorHandler(&tiff_error_handler);
	TIFFSetWarningHandler(&tiff_warning_handler);
	tif = TIFFOpen(filename.c_str(), "r");
	if (!tif) {
		return nullptr;
	}
	TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &W);
	TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &H);
	uint32_t npixels = W * H;
	uint32_t *raster = (uint32_t *)_TIFFmalloc(npixels * sizeof(uint32_t));
	int result = TIFFReadRGBAImage(tif, W, H, raster, 0);
	if (result == 0) {
		console->error("Some error in TIFFReadRGBAImage :(");
		_TIFFfree(raster);
		XTIFFClose(tif);
		return nullptr;
	}

	console->info("Image dimensions : {} x {}", W, H);
	console->info("Constructing my image data structure ...");
	auto img = make_unique<Image>(W, H, reinterpret_cast<unsigned char*>(raster));
	_TIFFfree(raster);
	TIFFClose(tif);
	img->flip_y();
	return img;
}
